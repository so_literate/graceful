package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"sync"
	"time"

	"gitlab.com/so_literate/graceful"
)

func main() {
	// graceful setup

	// Maximum waiting time for all runners to shutdown.
	// Without timeout by default.
	graceful.TimeoutShutdown = time.Minute

	// Debug logger of the graceful system.
	// Just io.Writer.
	graceful.Log = log.Writer()

	log.Println("stop signals:", graceful.Signals) // syscall.SIGINT, syscall.SIGTERM by default

	server := NewServer()
	workers := NewWorkers(2)
	slowWorker := NewSlowWorker()

	// graceful will wait here:
	//  1. All runners done
	//  2. At least one runner return error
	//  3. Got Interrupt signal (graceful.Signals)
	err := graceful.Run(server, workers, slowWorker)
	if err != nil {
		log.Println("Shutdown with error:", err.Error())
		return
	}

	log.Println("Graceful shutdown")
}

// Server is a HTTP server.
type Server struct {
	server *http.Server
}

func NewServer() *Server {
	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello!\n")
	})

	return &Server{
		server: &http.Server{
			Addr:    ":8080",
			Handler: mux,
		},
	}
}

func (s *Server) Run(ctx context.Context) error {
	log.Println("Starting server on:", s.server.Addr)

	// run http server

	chanErr := make(chan error, 1)
	go func() {
		err := s.server.ListenAndServe()
		if err == nil || errors.Is(err, http.ErrServerClosed) {
			chanErr <- nil
			return
		}
		chanErr <- fmt.Errorf("server.ListenAndServe: %w", err)
	}()

	// waiting error or shutdown signal from <-ctx.Done()

	select {
	case err := <-chanErr:
		return err // when graceful got error then all runners got stop signal.

	case <-ctx.Done():
		log.Println("got shutdown signal")
		shutCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		err := s.server.Shutdown(shutCtx)
		if err != nil {
			return fmt.Errorf("server.Shutdown: %w", err)
		}
		log.Println("http server graceful shutdown")
	}

	return nil
}

// Workers contains some endless workers.
type Workers struct {
	ctx    context.Context
	number int
	wg     *sync.WaitGroup
}

func NewWorkers(number int) *Workers {
	return &Workers{
		number: number,
		wg:     new(sync.WaitGroup),
	}
}

func (w *Workers) work() {
	defer w.wg.Done()

	ticker := time.NewTicker(time.Duration(int64(time.Second)*rand.Int63n(30) + int64(time.Second)))

	for {
		select {
		case <-w.ctx.Done():
			log.Println("Worker got stop signal")
			return

		case workTime := <-ticker.C:
			log.Printf("Worker printed this at: %s", workTime.Format(time.RFC3339))
		}
	}
}

// Run runs all workers and waiting for everyone to finish their work.
func (w *Workers) Run(ctx context.Context) error {
	w.ctx = ctx
	w.wg.Add(w.number)

	for i := 0; i < w.number; i++ {
		go w.work()
	}

	w.wg.Wait()
	return nil
}

// SlowWorker just worker did his job after 10 seconds.
type SlowWorker struct{}

func NewSlowWorker() *SlowWorker {
	return new(SlowWorker)
}

func (sw *SlowWorker) Run(ctx context.Context) error {
	timer := time.NewTimer(time.Second * 10)

	select {
	case <-ctx.Done():
		log.Println("SlowWorker got stop signal, well...")

		time.Sleep(time.Second * 5) // long shutdown, you can press Ctrl+C twice to sent force stop signal.
		return fmt.Errorf("SlowWorker was interrupted without work")

	case workTime := <-timer.C:
		log.Printf("SlowWorker done at: %s", workTime.Format(time.RFC3339))
	}

	// you can return nil from runner.
	return nil
}
