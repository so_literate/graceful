package graceful_test

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"gitlab.com/so_literate/graceful"
)

type ctxKey int

const ctxKeyTest ctxKey = 1

var errTest = errors.New("test error")

func TestError(t *testing.T) {
	err := &graceful.Error{
		Force:   graceful.ErrForceShutdown,
		Timeout: graceful.ErrTimeoutShutdown,
		Runners: []error{errTest, fmt.Errorf("err2: %w", errTest)},
	}

	expErrText := fmt.Sprintf("%s | %s | runners: [(0:%s),(1:err2: %s)]",
		graceful.ErrForceShutdown, graceful.ErrTimeoutShutdown, errTest, errTest,
	)

	if expErrText != err.Error() {
		t.Fatalf("wrong text error:\nwant:\t'%s'\ngot:\t'%s'", expErrText, err.Error())
	}
}

func assertError(t *testing.T, actualErr, force, timeout error, runners []error) {
	t.Helper()
	gErr := new(graceful.Error)
	if !errors.As(actualErr, &gErr) {
		t.Fatalf("wrong error: %s", actualErr.Error())
	}

	if !errors.Is(gErr.Force, force) {
		t.Fatalf("wrong force error\nwant:\t'%#v'\ngot:\t'%#v'", force, gErr.Force)
	}

	if !errors.Is(gErr.Timeout, timeout) {
		t.Fatalf("wrong timeout error\nwant:\t'%#v'\ngot:\t'%#v'", timeout, gErr.Timeout)
	}

	if len(gErr.Runners) != len(runners) {
		t.Fatalf("wrong length of runners errors\nwant:\t'%d'\ngot:\t'%d'", len(runners), len(gErr.Runners))
	}

	for i, want := range runners {
		got := gErr.Runners[i]

		if !errors.Is(got, want) {
			t.Fatalf("wrong runner error [%d]\nwant:\t'%#v'\ngot:\t'%#v'", i, want, got)
		}
	}
}

func printLog(t *testing.T, buf *bytes.Buffer) {
	t.Helper()
	scanner := bufio.NewScanner(buf)
	for scanner.Scan() {
		t.Log(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		t.Fatalf("failed to read buf log: %s", err)
	}
}

type runner struct {
	ctx      context.Context
	runnedAt time.Time
	stopedAt time.Time
}

func (r *runner) Run(ctx context.Context) error {
	r.runnedAt = time.Now()

	r.ctx = ctx
	<-ctx.Done()

	r.stopedAt = time.Now()

	return nil
}

type runnerNoErr struct {
	runnedAt time.Time
	stopedAt time.Time
}

func (r *runnerNoErr) Run(ctx context.Context) {
	r.runnedAt = time.Now()

	<-ctx.Done()

	r.stopedAt = time.Now()
}

func TestGracefulRun(t *testing.T) {
	r := new(runner)

	rNoErr := new(runnerNoErr)

	buf := bytes.NewBuffer(nil)
	graceful.Log = buf

	go func() {
		time.Sleep(time.Second)
		graceful.Interrupt <- graceful.Signals[0]
	}()

	err := graceful.Run(r, graceful.NoErr(rNoErr.Run))
	if err != nil {
		t.Fatalf("error after graceful: %s", err)
	}

	printLog(t, buf)

	if r.runnedAt.After(r.stopedAt) {
		t.Fatalf("wrong time of runner: %s > %s",
			r.runnedAt.Format(time.RFC3339), r.stopedAt.Format(time.RFC3339),
		)
	}

	if rNoErr.runnedAt.After(rNoErr.stopedAt) {
		t.Fatalf("wrong time of no err runner: %s > %s",
			rNoErr.runnedAt.Format(time.RFC3339), rNoErr.stopedAt.Format(time.RFC3339),
		)
	}
}

func TestGracefulRunContext(t *testing.T) {
	r := new(runner)
	ctx := context.WithValue(context.Background(), ctxKeyTest, 1)

	go func() {
		time.Sleep(time.Second)
		graceful.Interrupt <- graceful.Signals[0]
	}()

	err := graceful.RunContext(ctx, r)
	if err != nil {
		t.Fatalf("error after graceful: %s", err)
	}

	value, ok := r.ctx.Value(ctxKeyTest).(int)
	if !ok {
		t.Fatalf("wrong type of value in context: %#v", r.ctx.Value(ctxKeyTest))
	}

	if value != 1 {
		t.Fatalf("wrong value in context: %d", value)
	}
}

type runnerWithError struct{}

func (r *runnerWithError) Run(ctx context.Context) error {
	return errTest
}

func TestRunnerWithError(t *testing.T) {
	r := new(runnerWithError)

	buf := bytes.NewBuffer(nil)
	graceful.Log = buf

	err := graceful.RunFuncs(r.Run)
	if err == nil {
		t.Fatalf("want error, got nil")
	}

	printLog(t, buf)
	assertError(t, err, nil, nil, []error{errTest})
}

type runnerSlow struct{}

func (r *runnerSlow) Run(ctx context.Context) error {
	<-ctx.Done()
	time.Sleep(time.Millisecond * 800)
	return nil
}

func TestTimeout(t *testing.T) {
	r := new(runnerSlow)

	buf := bytes.NewBuffer(nil)
	graceful.Log = buf

	graceful.TimeoutShutdown = time.Millisecond * 500

	go func() {
		time.Sleep(time.Second)
		graceful.Interrupt <- graceful.Signals[1]
	}()

	err := graceful.Run(r)
	if err == nil {
		t.Fatalf("want error, got nil")
	}

	printLog(t, buf)
	assertError(t, err, nil, graceful.ErrTimeoutShutdown, nil)
}

func TestSlowTimeout(t *testing.T) {
	r := new(runnerSlow)

	buf := bytes.NewBuffer(nil)
	graceful.Log = buf

	graceful.TimeoutShutdown = time.Second // runnerSlow will be faster

	go func() {
		time.Sleep(time.Second)
		graceful.Interrupt <- graceful.Signals[1]
	}()

	err := graceful.Run(r)
	if err != nil {
		t.Fatalf("error after graceful: %s", err)
	}

	printLog(t, buf)
}

func TestForceStop(t *testing.T) {
	r := new(runnerSlow)

	buf := bytes.NewBuffer(nil)
	graceful.Log = buf

	graceful.TimeoutShutdown = 0

	go func() {
		time.Sleep(time.Second)
		graceful.Interrupt <- graceful.Signals[0]
		time.Sleep(time.Millisecond * 500) // faster then runnerSlow
		graceful.Interrupt <- graceful.Signals[0]
	}()

	err := graceful.Run(r)
	if err == nil {
		t.Fatalf("want error, got nil")
	}

	printLog(t, buf)
	assertError(t, err, graceful.ErrForceShutdown, nil, nil)
}

type runnerEnded struct{}

func (r *runnerEnded) Run(ctx context.Context) error {
	time.Sleep(time.Millisecond * 500)
	return nil
}

func TestKeepWorkWithEndedRunner(t *testing.T) {
	r := new(runner)
	re := new(runnerEnded) // return nil of this runner shouldn't affect the work of others.

	buf := bytes.NewBuffer(nil)
	graceful.Log = buf

	graceful.TimeoutShutdown = 0

	go func() {
		time.Sleep(time.Second)
		graceful.Interrupt <- graceful.Signals[0]
	}()

	start := time.Now()
	err := graceful.Run(r, re)
	runTime := time.Since(start)

	if err != nil {
		t.Fatalf("error after graceful: %s", err)
	}

	printLog(t, buf)

	if runTime < time.Second {
		t.Fatal("runner ended before interrupt signal")
	}
}

func TestAnyRunnersWithTimeout(t *testing.T) {
	r := new(runner)
	rs := new(runnerSlow)
	re := new(runnerWithError)

	graceful.Log = nil // without log

	graceful.TimeoutShutdown = time.Millisecond * 500

	go func() {
		// runnerWithError will be return error
		time.Sleep(time.Millisecond * 800) // timeout will be faster
		graceful.Interrupt <- graceful.Signals[0]
	}()

	err := graceful.Run(r, rs, re)
	if err == nil {
		t.Fatalf("want error, got nil")
	}

	if r.runnedAt.After(r.stopedAt) {
		t.Fatalf("wrong time of runner: %s > %s",
			r.runnedAt.Format(time.RFC3339), r.stopedAt.Format(time.RFC3339),
		)
	}

	assertError(t, err, nil, graceful.ErrTimeoutShutdown, []error{errTest})
}
