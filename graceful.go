// Package graceful contains runner with graceful
// shutdown system on error or on interrupt signals.
package graceful

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"
)

var (
	// TimeoutShutdown - time to wait for shutdown all runners.
	// Default without timeout.
	TimeoutShutdown time.Duration

	// Signals list of signals to stop runners.
	Signals = []os.Signal{syscall.SIGINT, syscall.SIGTERM}

	// Interrupt channel of the signals to handle stop command.
	Interrupt = make(chan os.Signal, 1)

	// Log is a writer to print events messages.
	Log io.Writer

	// logger is a initialized logger over by Log writer.
	logger *log.Logger
	once   sync.Once
)

// Runner is a runner with context to graceful shutdown.
type Runner interface {
	// Run runs an object and waits for it to complete with or without error.
	// Takes context with Cancel. Object have to wait ctx.Done in this method.
	Run(ctx context.Context) error
}

// runnerNoErr is a runner with context to graceful shutdown without error.
type runnerNoErr struct {
	Runner func(ctx context.Context)
}

// Run implements Runner interface with nil error.
func (r *runnerNoErr) Run(ctx context.Context) error {
	r.Runner(ctx)
	return nil
}

// NoErr wraps runner without error to runner with nil error.
func NoErr(runner func(ctx context.Context)) Runner {
	return &runnerNoErr{Runner: runner}
}

var (
	// ErrForceShutdown returns when gracy got double interrupt signal.
	ErrForceShutdown = errors.New("force shutdown signal")
	// ErrTimeoutShutdown returns when graceful shutdown timed out.
	ErrTimeoutShutdown = errors.New("graceful shutdown timeout expired")
)

// Error is a error of graceful shutdown.
type Error struct {
	Force   error   // stopped by froce comand
	Timeout error   // stopped by timeout
	Runners []error // some errors from runners
}

// Error implements error interface.
func (e *Error) Error() string {
	errs := make([]string, 0, 3)

	if e.Force != nil {
		errs = append(errs, e.Force.Error())
	}

	if e.Timeout != nil {
		errs = append(errs, e.Timeout.Error())
	}

	if len(e.Runners) != 0 {
		runners := make([]string, len(e.Runners))
		for i, err := range e.Runners {
			runners[i] = fmt.Sprintf("(%d:%s)", i, err.Error())
		}

		errs = append(errs, "runners: ["+strings.Join(runners, ",")+"]")
	}

	return strings.Join(errs, " | ")
}

func logPrintf(format string, args ...interface{}) {
	if Log == nil {
		return
	}

	once.Do(func() {
		logger = log.New(Log, "[graceful]: ", 0)
	})

	logger.Printf(format+"\n", args...)
}

type gracy struct {
	forceErr   error
	timeoutErr error
	runnersErr []error

	ctx    context.Context
	cancel context.CancelFunc

	wg *sync.WaitGroup

	runnerErr chan error
	exit      chan struct{}
}

func (g *gracy) waitSignalOrError() {
	gotStopSignal := false

	for {
		select {
		case <-g.exit:
			return

		case runErr := <-g.runnerErr:
			if runErr != nil {
				logPrintf("got error from runner: %s", runErr)
				g.runnersErr = append(g.runnersErr, runErr)

			}

			g.wg.Done()

			if runErr == nil || gotStopSignal {
				continue
			}

		case sig := <-Interrupt:
			logPrintf("got stop signal: %s", sig)

			if gotStopSignal {
				logPrintf("force stop")
				g.forceErr = fmt.Errorf("%w: %s", ErrForceShutdown, sig)
				close(g.exit)
				return
			}
		}

		gotStopSignal = true
		g.cancel()
	}
}

func (g *gracy) run(fn func(context.Context) error) {
	g.runnerErr <- fn(g.ctx)
}

func (g *gracy) waitShutdownOrTimeout() {
	go func() { // wait group of runners
		g.wg.Wait()

		logPrintf("all runners done")
		close(g.exit)
	}()

	go func() { // wait cancel of the context
		<-g.ctx.Done()

		if TimeoutShutdown == 0 {
			return
		}

		select {
		case <-g.exit:
			return
		case <-time.NewTimer(TimeoutShutdown).C:
		}

		g.timeoutErr = fmt.Errorf("%w: timeout: %s", ErrTimeoutShutdown, TimeoutShutdown.String())

		logPrintf("shutdown timeout expired")
		close(g.exit)
	}()
}

func (g *gracy) makeError() error {
	if g.forceErr == nil && g.timeoutErr == nil && len(g.runnersErr) == 0 {
		return nil
	}

	return &Error{
		Force:   g.forceErr,
		Timeout: g.timeoutErr,
		Runners: g.runnersErr,
	}
}

// RunFuncsContext runs all runners functions with special context.
func RunFuncsContext(ctx context.Context, funcs ...func(context.Context) error) error {
	cCtx, cancel := context.WithCancel(ctx)

	wg := new(sync.WaitGroup)
	wg.Add(len(funcs))

	g := &gracy{
		ctx:       cCtx,
		cancel:    cancel,
		wg:        wg,
		runnerErr: make(chan error),
		exit:      make(chan struct{}),
	}

	signal.Notify(Interrupt, Signals...)

	go g.waitSignalOrError()

	for _, fn := range funcs {
		go g.run(fn)
	}

	g.waitShutdownOrTimeout()

	<-g.exit

	return g.makeError()
}

// RunFuncs run all runner funcs with background context.
func RunFuncs(funcs ...func(context.Context) error) error {
	return RunFuncsContext(context.Background(), funcs...)
}

// RunContext run all passed runners with special context.
func RunContext(ctx context.Context, runners ...Runner) error {
	funcs := make([]func(context.Context) error, len(runners))
	for i, runner := range runners {
		funcs[i] = runner.Run
	}

	return RunFuncsContext(ctx, funcs...)
}

// Run runs all passed runners with background context.
func Run(runners ...Runner) error {
	return RunContext(context.Background(), runners...)
}
